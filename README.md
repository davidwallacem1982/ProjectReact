# Projeto React com Vercel e GitActions

Esse projeto foi criado para estudos de de uma aplicação em React usando o Vercel para fazer postagens automática utilizando também a funcionalidade do GitActions.

## Link do Projeto postado no Vercel

Aqui você encontra um projeto que foi postado no [Vercel](https://vercel.com) então é só você
clicar em um dos links abaixo para abrir o projeto postado:

### Produção

[link da Master](https://project-react-git-master-davidwallacem.vercel.app)

### Implantar

[link de Deploy](https://project-react-git-deploy-davidwallacem.vercel.app)

### Desenvolvimento

[link de Develop](https://project-react-git-develop-davidwallacem.vercel.app)

## Usuário para logar no projeto

**Login:** `teste@email.com`
**Senha:** `1234`

## Últimas observações

Lembrando que esse projeto é só um projeto de estudo então todas as informações que estão nele são fictícia e algumas funcionalidades dele não funcionam corretamente por que é só um projeto para testar as algumas funcionalidades do [React](https://pt-br.reactjs.org/), [Vercel](https://vercel.com) e [GitHub](https://github.com);
